# Fitting noisy signal with sinus

This project shows how to fit noisy signal with function <code>f(x) = sin(w*x) + b</code>.

The "data" and plotting functions are taken from https://habr.com/ru/company/ods/blog/322076/

Just run <code>python regression.py</code> and see result.

# Requirements

* Python version >= 3.7
* numpy vesion >= 1.17.4
* matplotlib version >= 3.2.1
