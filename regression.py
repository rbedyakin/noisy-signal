import numpy as np
import matplotlib.pyplot as plt

# for reproducible results
np.random.seed(42)


def generate_wave_set(n_support=1000, n_train=25, std=0.3):
    '''
    Example taken from https://habr.com/ru/company/ods/blog/322076/
    '''
    data = {}
    # выберем некоторое количество точек из промежутка от 0 до 2*pi
    data['support'] = np.linspace(0, 2 * np.pi, num=n_support)
    # для каждой посчитаем значение sin(x) + 1
    # это будет ground truth
    data['values'] = np.sin(data['support']) + 1
    # из support посемплируем некоторое количество точек с возвратом, это будут признаки
    data['x_train'] = np.sort(
        np.random.choice(data['support'], size=n_train, replace=True))
    # опять посчитаем sin(x) + 1 и добавим шум, получим целевую переменную
    data['y_train'] = np.sin(data['x_train']) + 1 + np.random.normal(
        0, std, size=data['x_train'].shape[0])
    return data


data = generate_wave_set(1000, 250)

# Plot true manifold and noised data
# plotting function taken from https://habr.com/ru/company/ods/blog/322076/

print('Shape of X is', data['x_train'].shape)
print('Head of X is', data['x_train'][:10])

margin = 0.3
plt.plot(data['support'], data['values'], 'b--', alpha=0.5, label='manifold')
plt.scatter(data['x_train'],
            data['y_train'],
            40,
            'g',
            'o',
            alpha=0.8,
            label='data')
plt.xlim(data['x_train'].min() - margin, data['x_train'].max() + margin)
plt.ylim(data['y_train'].min() - margin, data['y_train'].max() + margin)
plt.legend(loc='upper right', prop={'size': 20})
plt.title('True manifold and noised data')
plt.xlabel('x')
plt.ylabel('y')
plt.show()

# Learning

# Set hyperparameters
NUM_EPOCH = 100
LEARNING_RATE = 0.0005

# Set initial values
w = 0.05 * np.random.rand()
b = 0.05 * np.random.rand()

print(f'Initial values: w = {w}, b = {b}')

# Train loop
for epoch in range(NUM_EPOCH):
    y_hat = np.sin(w * data['x_train']) + b

    # Calc loss and gradients
    loss = np.sum((y_hat - data['y_train'])**2)
    grad_w = np.sum(2.0 * (y_hat - data['y_train']) * data['x_train'] *
                    np.cos(w * data['x_train']))
    grad_b = np.sum(2.0 * (y_hat - data['y_train']))

    # Gradient descent
    w -= LEARNING_RATE * grad_w
    b -= LEARNING_RATE * grad_b

print(f'Learned values: w = {w}, b = {b}')

y_hat = np.sin(w * data['x_train']) + b

# Plot results
# plotting function taken from https://habr.com/ru/company/ods/blog/322076/
margin = 0.3
plt.plot(data['support'], data['values'], 'b--', alpha=0.5, label='manifold')
plt.scatter(data['x_train'],
            data['y_train'],
            40,
            'g',
            'o',
            alpha=0.8,
            label='data')

plt.plot(data['x_train'], y_hat, 'r', alpha=0.8, label='fitted')

plt.xlim(data['x_train'].min() - margin, data['x_train'].max() + margin)
plt.ylim(data['y_train'].min() - margin, data['y_train'].max() + margin)
plt.legend(loc='upper right', prop={'size': 20})
plt.title('Fitted regression')
plt.xlabel('x')
plt.ylabel('y')
plt.show()
